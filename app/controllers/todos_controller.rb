class TodosController < ApplicationController
  before_action :set_todo, only: [:show, :update, :destroy]

  # GET /api/todos
  def index
    @todos = current_user.todos
    json_response(@todos)
  end

  # POST /api/todos
  def create
    @todo = current_user.todos.create!(todo_params)
    json_response(@todo, :created)
  end

  # GET /api/todos/:id
  def show
    json_response(@todo)
  end

  # PUT /api/todos/:id
  def update
    @todo.update(todo_params)
    head :no_content
  end

  # DELETE /api/todos/:id
  def destroy
    @todo.destroy
    head :no_content
  end

  private

  def todo_params
    params.permit(:title)
  end

  def set_todo
    @todo = Todo.find(params[:id])
  end
end
